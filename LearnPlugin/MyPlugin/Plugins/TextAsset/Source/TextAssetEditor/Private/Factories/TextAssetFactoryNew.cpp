// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "TextAssetFactoryNew.h"

#include "TextAsset.h"


/* UTextAssetFactoryNew structors
 *****************************************************************************/

UTextAssetFactoryNew::UTextAssetFactoryNew(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	//初始化工作，告诉工厂支持UTextAsset
	SupportedClass = UTextAsset::StaticClass();
	//新建一个实例，而不是拖放资源
	bCreateNew = true;
	//创建资源后，可以编辑资源的名称
	bEditAfterNew = true;
}


/* UFactory overrides
 创建TextAsset时，这个函数就会调用*/

UObject* UTextAssetFactoryNew::FactoryCreateNew(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn)
{
	return NewObject<UTextAsset>(InParent, InClass, InName, Flags);
}

/* 让文本资源出现在菜单中 */
bool UTextAssetFactoryNew::ShouldShowInNewMenu() const
{
	return true;
}
