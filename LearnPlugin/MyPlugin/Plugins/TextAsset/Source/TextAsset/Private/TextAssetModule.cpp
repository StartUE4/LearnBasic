// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Modules/ModuleInterface.h"
#include "Modules/ModuleManager.h"


/**
 * Implements the TextAsset module.
 */
class FTextAssetModule
	: public IModuleInterface
{
public:

	//~ IModuleInterface interface
//引擎每次把模块加载到内存中时被执行，在这里可以执行模块需要的初始化内容
	virtual void StartupModule() override { }
	//会在模块从内存中卸载时被执行，通常会在最后引擎关闭时发生
	virtual void ShutdownModule() override { }

	virtual bool SupportsDynamicReloading() override
	{
		return true;
	}
};


IMPLEMENT_MODULE(FTextAssetModule, TextAsset);
