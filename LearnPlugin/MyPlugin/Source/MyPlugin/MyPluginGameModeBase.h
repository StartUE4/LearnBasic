// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyPluginGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MYPLUGIN_API AMyPluginGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
